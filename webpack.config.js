const { join } = require('path');

module.exports = {
  entry: join(__dirname, 'src', 'index.jsx'),
  output: {
    filename: 'index.js',
    path: join(__dirname, 'dist'),
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.jsx', '.js'],
  },
  devServer: {
    contentBase: join(__dirname),
    hot: true,
  },
  module: {
    rules: [
      { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader' },
    ],
  },
};
