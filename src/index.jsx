import React from 'react';
import ReactDOM from 'react-dom';

const red = {
  color: 'red',
};

const App = () => <div style={red}>Hola, Lola!</div>;

ReactDOM.render(<App />, document.getElementById('app'));
